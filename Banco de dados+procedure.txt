


/* Drop Triggers */

DROP TRIGGER TRI_TB_consulta_usuario_id_consulta;
DROP TRIGGER TRI_TB_conversa_id_conversa;
DROP TRIGGER TRI_TB_municipio_id_municipio;
DROP TRIGGER TRI_TB_usuario_id_usuario;

/* Drop Triggers */

DROP TRIGGER TRI_TB_cons_usuario_id_cons;
DROP TRIGGER TRI_TB_conversa_id_conversa;
DROP TRIGGER TRI_TB_municipio_id_municipio;
DROP TRIGGER TRI_TB_usuario_id_usuario;



/* Drop Tables */

DROP TABLE TB_consulta CASCADE CONSTRAINTS;
DROP TABLE TB_consulta_usuario CASCADE CONSTRAINTS;
DROP TABLE TB_mensagem CASCADE CONSTRAINTS;
DROP TABLE TB_conversa_usuario CASCADE CONSTRAINTS;
DROP TABLE TB_conversa CASCADE CONSTRAINTS;
DROP TABLE TB_usuario CASCADE CONSTRAINTS;
DROP TABLE TB_municipio CASCADE CONSTRAINTS;
DROP TABLE TB_estado CASCADE CONSTRAINTS;
DROP TABLE TB_pais CASCADE CONSTRAINTS;



/* Drop Sequences */
DROP SEQUENCE SEQ_TB_cons_usuario_id_cons;
DROP SEQUENCE SEQ_TB_conversa_id_conversa;
DROP SEQUENCE SEQ_TB_municipio_id_municipio;
DROP SEQUENCE SEQ_TB_usuario_id_usuario;




/* Create Sequences */
CREATE SEQUENCE SEQ_TB_cons_usuario_id_cons INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_TB_conversa_id_conversa INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_TB_municipio_id_municipio INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_TB_usuario_id_usuario INCREMENT BY 1 START WITH 1;



/* Create Tables */

CREATE TABLE TB_consulta
(
	id_consulta number(10,0) NOT NULL,
	id_usuario number(10,0),
	id_municipio number(10,0),
	id_conversa number(10,0),
	id_mensagem number(10,0),
	id_estado number(10,0),
	nm_usuario varchar2(120),
	nick varchar2(40),
	email varchar2(64),
	senha varchar2(40),
	banido char,
	moderador char,
	corpo_mensagem varchar2(1000),
	dt_envio timestamp
);


CREATE TABLE TB_consulta_usuario
(
	id_consulta number(10,0) NOT NULL,
	id_usuario_pediu number(10,0) NOT NULL,
	dt_consulta timestamp DEFAULT SYSDATE,
	desc_consulta varchar2(200),
	nm_consulta varchar2(100),
	PRIMARY KEY (id_consulta)
);


CREATE TABLE TB_conversa
(
	id_conversa number(10,0) NOT NULL,
	nm_conversa varchar2(40),
	PRIMARY KEY (id_conversa)
);


CREATE TABLE TB_conversa_usuario
(
	id_usuario number(10,0) NOT NULL,
	id_conversa number(10,0) NOT NULL,
	banido char,
	moderador char,
	PRIMARY KEY (id_usuario, id_conversa)
);


CREATE TABLE TB_estado
(
	id_estado number(10,0) NOT NULL,
	nm_estado varchar2(30),
	sigla varchar2(2),
	id_pais number(10,0) NOT NULL,
	PRIMARY KEY (id_estado)
);


CREATE TABLE TB_mensagem
(
	id_mensagem number(10,0) NOT NULL,
	id_usuario number(10,0) NOT NULL,
	id_conversa number(10,0) NOT NULL,
	corpo_mensagem varchar2(1000),
	dt_envio timestamp DEFAULT SYSDATE,
	PRIMARY KEY (id_mensagem)
);


CREATE TABLE TB_municipio
(
	id_municipio number(10,0) NOT NULL,
	nm_municipio varchar2(40),
	id_estado number(10,0) NOT NULL,
	PRIMARY KEY (id_municipio)
);


CREATE TABLE TB_pais
(
	id_pais number(10,0) NOT NULL,
	nm_pais varchar2(50),
	PRIMARY KEY (id_pais)
);


CREATE TABLE TB_usuario
(
	id_usuario number(10,0) NOT NULL,
	nm_usuario varchar2(120),
	nick varchar2(40),
	email varchar2(64),
	senha varchar2(40),
	id_municipio number(10,0) NOT NULL,
	PRIMARY KEY (id_usuario)
);



/* Create Foreign Keys */

ALTER TABLE TB_consulta
	ADD FOREIGN KEY (id_consulta)
	REFERENCES TB_consulta_usuario (id_consulta)
;


ALTER TABLE TB_conversa_usuario
	ADD FOREIGN KEY (id_conversa)
	REFERENCES TB_conversa (id_conversa)
;


ALTER TABLE TB_mensagem
	ADD FOREIGN KEY (id_usuario, id_conversa)
	REFERENCES TB_conversa_usuario (id_usuario, id_conversa)
;


ALTER TABLE TB_municipio
	ADD FOREIGN KEY (id_estado)
	REFERENCES TB_estado (id_estado)
;


ALTER TABLE TB_usuario
	ADD FOREIGN KEY (id_municipio)
	REFERENCES TB_municipio (id_municipio)
;


ALTER TABLE TB_estado
	ADD FOREIGN KEY (id_pais)
	REFERENCES TB_pais (id_pais)
;


ALTER TABLE TB_consulta_usuario
	ADD FOREIGN KEY (id_usuario_pediu)
	REFERENCES TB_usuario (id_usuario)
;


ALTER TABLE TB_conversa_usuario
	ADD FOREIGN KEY (id_usuario)
	REFERENCES TB_usuario (id_usuario)
;



/* Create Triggers */



CREATE OR REPLACE TRIGGER TRI_TB_cons_usuario_id_cons BEFORE INSERT ON TB_consulta_usuario
FOR EACH ROW
BEGIN
	SELECT SEQ_TB_cons_usuario_id_cons.nextval
	INTO :new.id_consulta
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_TB_conversa_id_conversa BEFORE INSERT ON TB_conversa
FOR EACH ROW
BEGIN
	SELECT SEQ_TB_conversa_id_conversa.nextval
	INTO :new.id_conversa
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_TB_municipio_id_municipio BEFORE INSERT ON TB_municipio
FOR EACH ROW
BEGIN
	SELECT SEQ_TB_municipio_id_municipio.nextval
	INTO :new.id_municipio
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_TB_usuario_id_usuario BEFORE INSERT ON TB_usuario
FOR EACH ROW
BEGIN
	SELECT SEQ_TB_usuario_id_usuario.nextval
	INTO :new.id_usuario
	FROM dual;
END;

/







INSERT INTO TB_PAIS values(1,'Brasil');
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(1, 'Goi�s', 'GO',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(2, 'Minas Gerais', 'MG',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(3, 'Sao Paulo', 'SP',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(4, 'Parana', 'PR',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(5, 'Amapa', 'AP',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(6, 'Santa Catarina', 'SC',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(7, 'Rio Grande do Sul', 'RS',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(8, 'Rio Grande do Nort', 'RN',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(9, 'Para', 'PA',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(10, 'Pernambuco', 'PE',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(11, 'Espirito Santo', 'ES',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(12, 'Rio de Janeiro', 'RJ',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(13, 'Amazonas', 'AM',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(14, 'Maranhao', 'MA',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(15, 'Ceara', 'CE',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(16, 'Distrito Federal', 'DF',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(17, 'Mato Grosso', 'MT',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(18, 'Paraiba', 'PB',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(19, 'Alagoas', 'AL',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(20, 'Mato Grosso do Sul', 'MS',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(21, 'Acre', 'AC',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(22, 'Rondonia', 'RO',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(23, 'Sergipe', 'SE',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(24, 'Tocantins', 'TO',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(25, 'Piaui', 'PI',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES
(26, 'Roraima', 'RR',1);
INSERT INTO TB_ESTADO (id_estado, nm_estado, sigla,id_pais) VALUES (27, 'Bahia', 'BA',1);


INSERT INTO TB_MUNICIPIO (ID_MUNICIPIO, NM_MUNICIPIO, ID_ESTADO) VALUES ('1', 'Canoas', '7');
INSERT INTO TB_MUNICIPIO (ID_MUNICIPIO, NM_MUNICIPIO, ID_ESTADO) VALUES ('2', 'Porto Alegre', '7');
INSERT INTO TB_MUNICIPIO (ID_MUNICIPIO, NM_MUNICIPIO, ID_ESTADO) VALUES ('3', 'Esteio', '7');
INSERT INTO TB_MUNICIPIO (ID_MUNICIPIO, NM_MUNICIPIO, ID_ESTADO) VALUES ('4', 'Sao Leopoldo', '7');


INSERT INTO TB_USUARIO (ID_USUARIO, NM_USUARIO, NICK, EMAIL, SENHA, ID_MUNICIPIO) VALUES ('1', 'Gustavo Felipe Sa Brito', 'Gugafsb', 'gugafsb@gmail.com', '321huehue', '4');
INSERT INTO TB_USUARIO (ID_USUARIO, NM_USUARIO, NICK, EMAIL, SENHA, ID_MUNICIPIO) VALUES ('2', 'Larissa Bolzan Neves', 'Bola de neve', 'larihfennec@gmail.com', 'poneyland', '1');
INSERT INTO TB_USUARIO (ID_USUARIO, NM_USUARIO, NICK, EMAIL, SENHA, ID_MUNICIPIO) VALUES ('3', 'Guilherme Prezzi', 'Prezzi', 'prezzi@gmail.com', 'pagacantina', '3');
INSERT INTO TB_USUARIO (ID_USUARIO, NM_USUARIO, NICK, EMAIL, SENHA, ID_MUNICIPIO) VALUES ('4', 'Maria Luiza Wierzbicki Ribeiro', 'MaluRibeiro', 'maluwribeiro@gmail.com', 'mahvaiprala', '1');



INSERT INTO TB_CONVERSA (ID_CONVERSA, NM_CONVERSA) VALUES ('1', 'Amiguchos');



INSERT INTO TB_CONVERSA_USUARIO(ID_USUARIO,ID_CONVERSA,BANIDO,MODERADOR) VALUES(1,1,'N','Y');
INSERT INTO TB_CONVERSA_USUARIO(ID_USUARIO,ID_CONVERSA,BANIDO,MODERADOR) VALUES(2,1,'N','Y');
INSERT INTO TB_CONVERSA_USUARIO(ID_USUARIO,ID_CONVERSA,BANIDO,MODERADOR) VALUES(3,1,'Y','N');
INSERT INTO TB_CONVERSA_USUARIO(ID_USUARIO,ID_CONVERSA,BANIDO,MODERADOR) VALUES(4,1,'N','N');


INSERT INTO TB_MENSAGEM (ID_MENSAGEM, ID_CONVERSA, ID_USUARIO, CORPO_MENSAGEM, DT_ENVIO) VALUES ('1', '1', '1', 'Oi', TO_TIMESTAMP('2016-10-10 13:45:40.675000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO TB_MENSAGEM (ID_MENSAGEM, ID_CONVERSA, ID_USUARIO, CORPO_MENSAGEM, DT_ENVIO) VALUES ('2', '1', '2', 'Hey', TO_TIMESTAMP('2016-10-10 13:46:18.204000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO TB_MENSAGEM (ID_MENSAGEM, ID_CONVERSA, ID_USUARIO, CORPO_MENSAGEM, DT_ENVIO) VALUES ('3', '1', '3', 'hop', TO_TIMESTAMP('2016-10-10 13:46:43.398000000', 'YYYY-MM-DD HH24:MI:SS.FF'));
INSERT INTO TB_MENSAGEM (ID_MENSAGEM, ID_CONVERSA, ID_USUARIO, CORPO_MENSAGEM, DT_ENVIO) VALUES ('4', '1', '4', 'Ban', TO_TIMESTAMP('2016-10-10 13:47:33.494000000', 'YYYY-MM-DD HH24:MI:SS.FF'));



////////////////////////////////////////////////////////// testado e ta legal. A nossa tv pampa � o seu canal
CREATE OR REPLACE PROCEDURE ADICIONA_CONSULTA
(instrucao_sql IN varchar2,
 numero_usuario IN TB_CONSULTA_USUARIO.ID_USUARIO_PEDIU%TYPE,
 nome_consulta IN TB_CONSULTA_USUARIO.NM_CONSULTA%TYPE,
 descricao_consulta IN TB_CONSULTA_USUARIO.DESC_CONSULTA%TYPE
)
IS
BEGIN
INSERT INTO TB_CONSULTA_USUARIO(ID_USUARIO_PEDIU,DT_CONSULTA,DESC_CONSULTA,NM_CONSULTA) values(numero_usuario,CURRENT_TIMESTAMP,descricao_consulta,nome_consulta);
EXECUTE IMMEDIATE instrucao_sql;


END ADICIONA_CONSULTA;




////////////////////////////////////////////////// teste
exec ADICIONA_CONSULTA('SELECT * FROM TB_CONSULTA',1,'Teste','Testando');



////// Melhor usar esse insert, os outros ficam s� caso esse n�o funcione

exec ADICIONA_CONSULTA('INSERT INTO TB_CONSULTA (ID_CONSULTA) VALUES ((SELECT ID_CONSULTA FROM TB_CONSULTA_USUARIO WHERE DT_CONSULTA=(SELECT DT_CONSULTA
 FROM TB_CONSULTA_USUARIO WHERE ID_USUARIO_PEDIU='||numero_usuario||' AND WHERE rownum=1 desc)))',1,'Teste','Testando');




///// INSERT (caso o acima funcione, des)NECESS�RIO para o relat�rio
exec ADICIONA_CONSULTA('INSERT INTO TB_CONSULTA (ID_CONSULTA) VALUES ((SELECT ID_CONSULTA FROM TB_CONSULTA_USUARIO WHERE DT_CONSULTA=(SELECT MAX(DT_CONSULTA) FROM TB_CONSULTA_USUARIO WHERE ID_USUARIO_PEDIU='||numero_usuario||')))',1,'Teste','Testando');





 ///// INSERT comentado para facilitar o entendimento de como � o relat�ro
exec ADICIONA_CONSULTA('INSERT INTO TB_CONSULTA (ID_CONSULTA) VALUES ((SELECT ID_CONSULTA FROM TB_CONSULTA_USUARIO WHERE DT_CONSULTA=
                          'esse insert � utilizado para que saiba-se qual consulta est� sendo realizada, j� que � necessario primeiro
                          cri�-la na TB_CONSULTA_USUARIO e pegar o numero da consulta recem registrada para cadastrar uma consulta.'
(SELECT MAX(DT_CONSULTA) FROM TB_CONSULTA_USUARIO WHERE ID_USUARIO_PEDIU='||numero_usuario||')) )',1,'Teste','Testando');
                                                                                              													  a partir do penultimo par�ntese se coloca 
                                                                                             													  a inser�ao da consulta que o usu�rio decidir fazer
												                                                                                               utilizando o PHP. Posteriormente, ser� feito um
						
                                                                                               													SELECT retornando o resultado da inser��o na tb_consulta
												                                                                                               pois o NOLL disse que precisa fazer os 2 separado.