
/* Drop Triggers */

DROP TRIGGER TRI_TB_cons_usuario_id_cons;
DROP TRIGGER TRI_TB_conversa_id_conversa;
DROP TRIGGER TRI_TB_municipio_id_municipio;
DROP TRIGGER TRI_TB_usuario_id_usuario;



/* Drop Tables */

DROP TABLE TB_consulta CASCADE CONSTRAINTS;
DROP TABLE TB_consulta_usuario CASCADE CONSTRAINTS;
DROP TABLE TB_mensagem CASCADE CONSTRAINTS;
DROP TABLE TB_conversa_usuario CASCADE CONSTRAINTS;
DROP TABLE TB_conversa CASCADE CONSTRAINTS;
DROP TABLE TB_usuario CASCADE CONSTRAINTS;
DROP TABLE TB_municipio CASCADE CONSTRAINTS;
DROP TABLE TB_estado CASCADE CONSTRAINTS;
DROP TABLE TB_pais CASCADE CONSTRAINTS;



/* Drop Sequences */
DROP SEQUENCE SEQ_TB_cons_usuario_id_cons;
DROP SEQUENCE SEQ_TB_conversa_id_conversa;
DROP SEQUENCE SEQ_TB_municipio_id_municipio;
DROP SEQUENCE SEQ_TB_usuario_id_usuario;




/* Create Sequences */
CREATE SEQUENCE SEQ_TB_cons_usuario_id_cons INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_TB_conversa_id_conversa INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_TB_municipio_id_municipio INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_TB_usuario_id_usuario INCREMENT BY 1 START WITH 1;



/* Create Tables */

CREATE TABLE TB_consulta
(
	id_consulta number(10,0) NOT NULL,
	id_usuario number(10,0),
	id_municipio number(10,0),
	id_conversa number(10,0),
	id_mensagem number(10,0),
	id_estado number(10,0),
	nm_usuario varchar2(120),
	nick varchar2(40),
	email varchar2(64),
	senha varchar2(40),
	banido char,
	moderador char,
	corpo_mensagem varchar2(1000),
	dt_envio timestamp
);


CREATE TABLE TB_consulta_usuario
(
	id_consulta number(10,0) NOT NULL,
	id_usuario_pediu number(10,0) NOT NULL,
	dt_consulta timestamp DEFAULT SYSDATE,
	desc_consulta varchar2(200),
	nm_consulta varchar2(100),
	PRIMARY KEY (id_consulta)
);


CREATE TABLE TB_conversa
(
	id_conversa number(10,0) NOT NULL,
	nm_conversa varchar2(40),
	PRIMARY KEY (id_conversa)
);


CREATE TABLE TB_conversa_usuario
(
	id_usuario number(10,0) NOT NULL,
	id_conversa number(10,0) NOT NULL,
	banido char,
	moderador char,
	PRIMARY KEY (id_usuario, id_conversa)
);


CREATE TABLE TB_estado
(
	id_estado number(10,0) NOT NULL,
	nm_estado varchar2(30),
	sigla varchar2(2),
	id_pais number(10,0) NOT NULL,
	PRIMARY KEY (id_estado)
);


CREATE TABLE TB_mensagem
(
	id_mensagem number(10,0) NOT NULL,
	id_usuario number(10,0) NOT NULL,
	id_conversa number(10,0) NOT NULL,
	corpo_mensagem varchar2(1000),
	dt_envio timestamp DEFAULT SYSDATE,
	PRIMARY KEY (id_mensagem)
);


CREATE TABLE TB_municipio
(
	id_municipio number(10,0) NOT NULL,
	nm_municipio varchar2(40),
	id_estado number(10,0) NOT NULL,
	PRIMARY KEY (id_municipio)
);


CREATE TABLE TB_pais
(
	id_pais number(10,0) NOT NULL,
	nm_pais varchar2(50),
	PRIMARY KEY (id_pais)
);


CREATE TABLE TB_usuario
(
	id_usuario number(10,0) NOT NULL,
	nm_usuario varchar2(120),
	nick varchar2(40),
	email varchar2(64),
	senha varchar2(40),
	id_municipio number(10,0) NOT NULL,
	PRIMARY KEY (id_usuario)
);



/* Create Foreign Keys */

ALTER TABLE TB_consulta
	ADD FOREIGN KEY (id_consulta)
	REFERENCES TB_consulta_usuario (id_consulta)
;


ALTER TABLE TB_conversa_usuario
	ADD FOREIGN KEY (id_conversa)
	REFERENCES TB_conversa (id_conversa)
;


ALTER TABLE TB_mensagem
	ADD FOREIGN KEY (id_usuario, id_conversa)
	REFERENCES TB_conversa_usuario (id_usuario, id_conversa)
;


ALTER TABLE TB_municipio
	ADD FOREIGN KEY (id_estado)
	REFERENCES TB_estado (id_estado)
;


ALTER TABLE TB_usuario
	ADD FOREIGN KEY (id_municipio)
	REFERENCES TB_municipio (id_municipio)
;


ALTER TABLE TB_estado
	ADD FOREIGN KEY (id_pais)
	REFERENCES TB_pais (id_pais)
;


ALTER TABLE TB_consulta_usuario
	ADD FOREIGN KEY (id_usuario_pediu)
	REFERENCES TB_usuario (id_usuario)
;


ALTER TABLE TB_conversa_usuario
	ADD FOREIGN KEY (id_usuario)
	REFERENCES TB_usuario (id_usuario)
;



/* Create Triggers */



CREATE OR REPLACE TRIGGER TRI_TB_cons_usuario_id_cons BEFORE INSERT ON TB_consulta_usuario
FOR EACH ROW
BEGIN
	SELECT SEQ_TB_cons_usuario_id_cons.nextval
	INTO :new.id_consulta
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_TB_conversa_id_conversa BEFORE INSERT ON TB_conversa
FOR EACH ROW
BEGIN
	SELECT SEQ_TB_conversa_id_conversa.nextval
	INTO :new.id_conversa
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_TB_municipio_id_municipio BEFORE INSERT ON TB_municipio
FOR EACH ROW
BEGIN
	SELECT SEQ_TB_municipio_id_municipio.nextval
	INTO :new.id_municipio
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_TB_usuario_id_usuario BEFORE INSERT ON TB_usuario
FOR EACH ROW
BEGIN
	SELECT SEQ_TB_usuario_id_usuario.nextval
	INTO :new.id_usuario
	FROM dual;
END;

/




