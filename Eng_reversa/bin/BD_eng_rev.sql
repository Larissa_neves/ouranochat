SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS TB_mensagem;
DROP TABLE IF EXISTS TB_moderador;
DROP TABLE IF EXISTS TB_conversa;
DROP TABLE IF EXISTS TB_usuario;
DROP TABLE IF EXISTS TB_municipio;
DROP TABLE IF EXISTS TB_estado;
DROP TABLE IF EXISTS TB_pais;




/* Create Tables */

CREATE TABLE TB_conversa
(
	id_conversa int NOT NULL AUTO_INCREMENT,
	nm_conversa varchar(40),
	PRIMARY KEY (id_conversa)
);


CREATE TABLE TB_estado
(
	id_estado int NOT NULL,
	nm_estado varchar(30),
	sigla varchar(2),
	id_pais int NOT NULL,
	PRIMARY KEY (id_estado)
);


CREATE TABLE TB_mensagem
(
	id_mensagem int NOT NULL AUTO_INCREMENT,
	id_conversa int NOT NULL,
	id_usuario int NOT NULL,
	corpo_mensagem varchar(1000),
	dt_envio timestamp DEFAULT NOW(), SYSDATE(),
	PRIMARY KEY (id_mensagem)
);


CREATE TABLE TB_moderador
(
	id_usuario int NOT NULL,
	id_conversa int NOT NULL
);


CREATE TABLE TB_municipio
(
	id_municipio int NOT NULL,
	nm_municipio varchar(40),
	id_estado int NOT NULL,
	PRIMARY KEY (id_municipio)
);


CREATE TABLE TB_pais
(
	id_pais int NOT NULL AUTO_INCREMENT,
	nm_pais varchar(50),
	PRIMARY KEY (id_pais)
);


CREATE TABLE TB_usuario
(
	id_usuario int NOT NULL AUTO_INCREMENT,
	nm_usuario varchar(120),
	nick varchar(40),
	email varchar(64),
	senha varchar(40),
	id_municipio int NOT NULL,
	PRIMARY KEY (id_usuario)
);



/* Create Foreign Keys */

ALTER TABLE TB_mensagem
	ADD FOREIGN KEY (id_conversa)
	REFERENCES TB_conversa (id_conversa)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE TB_moderador
	ADD FOREIGN KEY (id_conversa)
	REFERENCES TB_conversa (id_conversa)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE TB_municipio
	ADD FOREIGN KEY (id_estado)
	REFERENCES TB_estado (id_estado)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE TB_usuario
	ADD FOREIGN KEY (id_municipio)
	REFERENCES TB_municipio (id_municipio)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE TB_estado
	ADD FOREIGN KEY (id_pais)
	REFERENCES TB_pais (id_pais)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE TB_mensagem
	ADD FOREIGN KEY (id_usuario)
	REFERENCES TB_usuario (id_usuario)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE TB_moderador
	ADD FOREIGN KEY (id_usuario)
	REFERENCES TB_usuario (id_usuario)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



