
/* Drop Tables */

DROP TABLE TB_banido CASCADE CONSTRAINTS;
DROP TABLE TB_consulta CASCADE CONSTRAINTS;
DROP TABLE TB_consulta_usuario CASCADE CONSTRAINTS;
DROP TABLE TB_mensagem CASCADE CONSTRAINTS;
DROP TABLE TB_moderador CASCADE CONSTRAINTS;
DROP TABLE TB_conversa CASCADE CONSTRAINTS;
DROP TABLE TB_usuario CASCADE CONSTRAINTS;
DROP TABLE TB_municipio CASCADE CONSTRAINTS;
DROP TABLE TB_estado CASCADE CONSTRAINTS;
DROP TABLE TB_pais CASCADE CONSTRAINTS;




/* Create Tables */

CREATE TABLE TB_banido
(
	id_usuario number(10,0) NOT NULL,
	id_conversa number(10,0) NOT NULL,
	mt_ban varchar2(300),
	PRIMARY KEY (id_usuario, id_conversa)
);


CREATE TABLE TB_consulta
(
	id_usuario_pediu number(10,0) NOT NULL,
	id_consulta number(10,0) NOT NULL,
	id_usuario number(10,0),
	id_municipio number(10,0),
	id_conversa number(10,0),
	id_mensagem number(10,0),
	id_estado number(10,0),
	nm_usuario varchar2(120),
	nick varchar2(40),
	email varchar2(64),
	senha varchar2(40),
	mt_ban varchar2(300),
	corpo_mensagem varchar2(1000),
	dt_envio timestamp
);


CREATE TABLE TB_consulta_usuario
(
	id_usuario number(10,0) NOT NULL,
	id_consulta number(10,0) NOT NULL,
	PRIMARY KEY (id_usuario, id_consulta)
);


CREATE TABLE TB_conversa
(
	id_conversa number(10,0) NOT NULL,
	nm_conversa varchar2(40),
	PRIMARY KEY (id_conversa)
);


CREATE TABLE TB_estado
(
	id_estado number(10,0) NOT NULL,
	nm_estado varchar2(30),
	sigla varchar2(2),
	id_pais number(10,0) NOT NULL,
	PRIMARY KEY (id_estado)
);


CREATE TABLE TB_mensagem
(
	id_mensagem number(10,0) NOT NULL,
	id_conversa number(10,0) NOT NULL,
	id_usuario number(10,0) NOT NULL,
	corpo_mensagem varchar2(1000),
	dt_envio timestamp DEFAULT SYSDATE,
	PRIMARY KEY (id_mensagem)
);


CREATE TABLE TB_moderador
(
	id_usuario number(10,0) NOT NULL,
	id_conversa number(10,0) NOT NULL,
	PRIMARY KEY (id_usuario, id_conversa)
);


CREATE TABLE TB_municipio
(
	id_municipio number(10,0) NOT NULL,
	nm_municipio varchar2(40),
	id_estado number(10,0) NOT NULL,
	PRIMARY KEY (id_municipio)
);


CREATE TABLE TB_pais
(
	id_pais number(10,0) NOT NULL,
	nm_pais varchar2(50),
	PRIMARY KEY (id_pais)
);


CREATE TABLE TB_usuario
(
	id_usuario number(10,0) NOT NULL,
	nm_usuario varchar2(120),
	nick varchar2(40),
	email varchar2(64),
	senha varchar2(40),
	id_municipio number(10,0) NOT NULL,
	PRIMARY KEY (id_usuario)
);



/* Create Foreign Keys */

ALTER TABLE TB_consulta
	ADD FOREIGN KEY (id_usuario_pediu, id_consulta)
	REFERENCES TB_consulta_usuario (id_usuario, id_consulta)
;


ALTER TABLE TB_banido
	ADD FOREIGN KEY (id_conversa)
	REFERENCES TB_conversa (id_conversa)
;


ALTER TABLE TB_mensagem
	ADD FOREIGN KEY (id_conversa)
	REFERENCES TB_conversa (id_conversa)
;


ALTER TABLE TB_moderador
	ADD FOREIGN KEY (id_conversa)
	REFERENCES TB_conversa (id_conversa)
;


ALTER TABLE TB_municipio
	ADD FOREIGN KEY (id_estado)
	REFERENCES TB_estado (id_estado)
;


ALTER TABLE TB_usuario
	ADD FOREIGN KEY (id_municipio)
	REFERENCES TB_municipio (id_municipio)
;


ALTER TABLE TB_estado
	ADD FOREIGN KEY (id_pais)
	REFERENCES TB_pais (id_pais)
;


ALTER TABLE TB_banido
	ADD FOREIGN KEY (id_usuario)
	REFERENCES TB_usuario (id_usuario)
;


ALTER TABLE TB_consulta_usuario
	ADD FOREIGN KEY (id_usuario)
	REFERENCES TB_usuario (id_usuario)
;


ALTER TABLE TB_mensagem
	ADD FOREIGN KEY (id_usuario)
	REFERENCES TB_usuario (id_usuario)
;


ALTER TABLE TB_moderador
	ADD FOREIGN KEY (id_usuario)
	REFERENCES TB_usuario (id_usuario)
;



