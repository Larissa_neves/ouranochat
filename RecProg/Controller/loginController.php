<?php
class loginController extends Controller {

    private $model;
    private $view;
    public function __construct() {
        $this->model = new loginModel();
        $this->view = new loginView;
    }

    public function telaLogin(){
        $this->view->exibirTelaLogin();
    }

     public function enviarMensagem(){
         $msg+=$_POST['msg'].'<br>';


     }


    public function fazerLogin(){
        if ($this->model->verificarSenhaUsuario($_POST['usuario'], $_POST['senha'])){
            $this->gravaSessao();
            $this->log('usuario logou');
            $this->view->exibirTelaLogado();
        }
        else{
            $this->log('usuario tentou logar mas nao conseguiu');
            $_GET['msg']='erro';
            $this->view->exibirTelaLogin();

        }
    }

    public function telaCadastro(){
        $this->view->exibirTelaCadastro();
    }

    public function fazerCadastro(){
        if (verificarUsuario($_POST['usuario'])){
           $this->log('usuario tentou cadastrar mas nao conseguiu');
            $this->view->exibirTelaErro();
        }
        else{
           //cadastrar
        }
    }

    private function gravaSessao(){
        $_SESSION['logado']= true;
        $_SESSION['nome']= $_POST['usuario'];
    }

    private function apagaSessao(){
        session_destroy();
        header(login.php);
    }

}
