<?php
class Controller {
    public function __construct() {
        
    }
    
    public function redirect($url){
    header('Location: '.$url);    
    }
    
    public function error ($error){
        $this->log($error);
    }
    
    public function log ($mensagem){
        $date = date('Y-m-d H:i:s');
        file_put_contents("log".$date,$mensagem);
    }
    
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

