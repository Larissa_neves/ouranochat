<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Relatorio</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">Ouranochat</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#">Navbar Link</a></li>
      </ul>

      <ul id="nav-mobile" class="side-nav">
        <li><a href="#">Navbar Link</a></li>
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
  </nav>
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <div class="input-field col s12">
        <form action="#">
          Usuario<br>

             <input type="checkbox" id="nm" />
             <label for="nm">Nome</label><br>

             <input type="checkbox" id="nick" />
             <label for="nick">Nick</label><br>

             <input type="checkbox" id="email" />
             <label for="email">Email</label><br><br>

          Conversa<br>

             <input type="checkbox" id="ban" />
             <label for="ban">Banido</label><br>

              <input type="checkbox" id="Mod" />
             <label for="Mod">Moderador</label><br>

             <input type="checkbox" id="idc" />
             <label for="idc">Conversa</label><br><br>

           Mensagem<br>

             <input type="checkbox" id="Cmen" />
             <label for="Cmen">Corpo da mensagem</label><br>

             <input type="checkbox" id="dt" />
             <label for="dt">Data de envio</label><br>

             <input type="checkbox" id="idcu" />
             <label for="idcu">Conversa</label><br><br>

           Municipio<br>

             <input type="checkbox" id="nmmu" />
             <label for="nmmu">Nome</label><br>

             <input type="checkbox" id="idmu" />
             <label for="idmu">Numero do município</label><br>

             <input type="checkbox" id="ide" />
             <label for="ide">Numero do Estado</label><br>



         </form>
      <br><br>
      <br><br>
    </div>
  </div>

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

      <script>
            $('select').material_select();
        });
      </script>
  </body>
</html>
