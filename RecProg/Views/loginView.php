<?php
class loginView extends View{
       private $view;
    function __construct() {
    $this->view = new View();
    }

    public function exibirTelaLogin(){
        $this->mostrarNaTela('login.php');
    }

    public function exibirTelaCadastro(){
        $this->mostrarNaTela('cadastro.htm');
    }

    public function exibirTelaErro(){
        $this->mostrarNaTela('erro.html');
    }

    public function exibirTelaLogado(){
        $this->mostrarNaTela('logado.php');
    }
}
